package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IProjectService {

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    void remove(Project project);

    Project add(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    Project findOneByIndex(Integer index);

    Project findOneById(String id);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project removeByIndex(Integer index);

    Project removeById(String id);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    void clear();

}
