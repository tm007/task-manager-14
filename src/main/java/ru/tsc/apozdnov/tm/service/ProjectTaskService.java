package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.service.IProjectTaskService;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public boolean bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return false;
        if (taskId == null || taskId.isEmpty()) return false;
        if (!projectRepository.existsById(projectId)) return false;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return false;
        task.setProjectId(projectId);
        return true;
    }

    @Override
    public boolean unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return false;
        if (taskId == null || taskId.isEmpty()) return false;
        if (!projectRepository.existsById(projectId)) return false;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return false;
        task.setProjectId(null);
        return true;
    }

    @Override
    public boolean removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return false;
        final Project project = projectRepository.findById(projectId);
        if (project == null) return false;
        projectRepository.remove(project);
        final List<Task> taskList = taskRepository.findAllByProjectId(projectId);
        for (final Task task : taskList) {
            taskRepository.removeById(task.getId());
        }
        projectRepository.removeById(projectId);
        return true;
    }

    @Override
    public boolean removeProject(final Project project) {
        if (project == null) return false;
        projectRepository.remove(project);
        final List<Task> taskList = taskRepository.findAllByProjectId(project.getId());
        for (Task task : taskList) {
            taskRepository.remove(task);
        }
        return true;
    }

}
