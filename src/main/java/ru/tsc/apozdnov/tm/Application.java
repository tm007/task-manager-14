package ru.tsc.apozdnov.tm;

import ru.tsc.apozdnov.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
